﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;
using System.IO;

namespace Sokoban
{
    class Level : Screen
    {
        // ------------------
        // Data
        // ------------------
        private Game1 game;
        private Tile[,] tiles;
        private Tile[,] floorTiles;
        private int currentLevel;
        private bool loadNextLevel = false;

        // Constants
        private const int LAST_LEVEL = 2;

        // Assets
        Texture2D wallSprite;
        Texture2D playerSprite;
        Texture2D boxSprite;
        Texture2D boxStoredSprite;
        Texture2D goalSprite;
        Texture2D floorSprite;

        // ------------------
        // Behaviour
        // ------------------
        public Level(Game1 newGame)
        {
            game = newGame;
        }
        // ------------------
        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            wallSprite = content.Load<Texture2D>("graphics/Wall");
            playerSprite = content.Load<Texture2D>("graphics/PlayerStatic");
            boxSprite = content.Load<Texture2D>("graphics/Box");
            boxStoredSprite = content.Load<Texture2D>("graphics/BoxStored");
            goalSprite = content.Load<Texture2D>("graphics/Goal");
            floorSprite = content.Load<Texture2D>("graphics/Floor");

            // TEMP - this will be moved later
            LoadLevel(1);
        }
        // ------------------
        public void LoadLevel(int levelNum)
        {
            currentLevel = levelNum;
            string baseLevelName = "LevelFiles/level_";
            LoadLevel(baseLevelName + levelNum.ToString() + ".txt");
        }
        // ------------------
        public void LoadLevel(string fileName)
        {
            // Clear any existing level data
            ClearLevel();

            // Create filestream to open the file and get it ready for reading
            Stream fileStream = TitleContainer.OpenStream(fileName);

            // Before we read in the individual tiles in the level, we need to know 
            // how big the level is overall to create the arrays to hold the data
            int lineWidth = 0; // Eventually will be levelWidth
            int numLines = 0;  // Eventually will be levelHeight
            List<string> lines = new List<string>();    // this will contain all the strings of text in the file
            StreamReader reader = new StreamReader(fileStream); // This will let us read each line from the file
            string line = reader.ReadLine(); // Get the first line
            lineWidth = line.Length; // Assume the overall line width is the same as the length of the first line
            while (line != null) // For as long as line exists, do something
            {
                lines.Add(line); // Add the current line to the list
                if (line.Length != lineWidth)
                {
                    // This means our lines are different sizes and that is a big problem
                    throw new Exception("Lines are different widths - error occured on line " + lines.Count);
                }

                // Read the next line to get ready for the next step in the loop
                line = reader.ReadLine();
            }

            // We have read in all the lines of the file into our lines list
            // We can now know how many lines there were
            numLines = lines.Count;

            // Now we can set up our tile array
            tiles = new Tile[lineWidth, numLines];
            floorTiles = new Tile[lineWidth, numLines];

            // Loop over every tile position and check the letter
            // there and load a tile based on  that letter
            for (int y = 0; y < numLines; ++y)
            {
                for (int x = 0; x < lineWidth; ++x)
                {
                    // Load each tile
                    char tileType = lines[y][x];
                    // Load the tile
                    LoadTile(tileType, x, y);
                }
            }
        }
        // ------------------
        private void LoadTile(char tileType, int tileX, int tileY)
        {
            switch (tileType)
            {
                // Wall
                case 'W':
                    CreateWall(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                // Player
                case 'P':
                    CreatePlayer(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                // Box
                case 'B':
                    CreateBox(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                // Goal
                case 'G':
                    CreateGoal(tileX, tileY);
                    break;

                // Blank space
                case '.':
                    CreateFloor(tileX, tileY);
                    break; // Do nothing

                // Any non-handled symbol
                default:
                    throw new NotSupportedException("Level contained unsupported symbol " + tileType + " at line " + tileY + " and character " + tileX);
            }
        }
        // ------------------
        private void ClearLevel()
        {
            // TODO
        }
        // ------------------
        private void CreateWall(int tileX, int tileY)
        {
            Wall tile = new Wall(wallSprite);
            tile.SetTilePosition(new Vector2(tileX, tileY));
            tiles[tileX, tileY] = tile;
        }
        // ------------------
        private void CreatePlayer(int tileX, int tileY)
        {
            Player tile = new Player(playerSprite, this);
            tile.SetTilePosition(new Vector2(tileX, tileY));
            tiles[tileX, tileY] = tile;
        }
        // ------------------
        private void CreateBox(int tileX, int tileY)
        {
            Box tile = new Box(boxSprite, boxStoredSprite, this);
            tile.SetTilePosition(new Vector2(tileX, tileY));
            tiles[tileX, tileY] = tile;
        }
        // ------------------
        private void CreateGoal(int tileX, int tileY)
        {
            Goal tile = new Goal(goalSprite);
            tile.SetTilePosition(new Vector2(tileX, tileY));
            floorTiles[tileX, tileY] = tile;
        }
        // ------------------
        private void CreateFloor(int tileX, int tileY)
        {
            Floor tile = new Floor(floorSprite);
            tile.SetTilePosition(new Vector2(tileX, tileY));
            floorTiles[tileX, tileY] = tile;
        }
        // ------------------
        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (Tile tile in floorTiles)
            {
                if (tile != null)
                    tile.Draw(spriteBatch);
            }
            foreach (Tile tile in tiles)
            {
                if (tile != null)
                    tile.Draw(spriteBatch);
            }
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            foreach (Tile tile in floorTiles)
            {
                if (tile != null)
                    tile.Update(gameTime);
            }
            foreach (Tile tile in tiles)
            {
                if (tile != null)
                    tile.Update(gameTime);
            }

            // If we were waiting to load a new level, do it now
            if (loadNextLevel == true)
            {
                if (currentLevel == LAST_LEVEL)
                {
                    LoadLevel(1); // restart so they can load again
                    game.ChangeScreen("win");
                }
                else
                {
                    LoadLevel(currentLevel + 1);
                }
                loadNextLevel = false;
            }
        }
        // ------------------
        public bool TryMoveTile(Tile toMove, Vector2 newPosition)
        {
            // Get the current tile position
            Vector2 currentTilePosition = toMove.GetTilePosition();

            // Check if the new position is within bounds
            int newPosX = (int)newPosition.X;
            int newPosY = (int)newPosition.Y;
            if (   newPosX >= 0 
                && newPosY >= 0 
                && newPosX < tiles.GetLength(0) // gets the length in the X direction
                && newPosY < tiles.GetLength(1)  ) // gets the array length in the Y direction
            {
                // Yes our new position is indeed legal

                // So let's actually move it.
                toMove.SetTilePosition(newPosition);

                // Move it to the correct place in the array,
                tiles[newPosX, newPosY] = toMove;

                // and remove it from the old place
                tiles[(int)currentTilePosition.X, (int)currentTilePosition.Y] = null;

                // We DID move it, so return true
                return true;
            }
            else
            {
                // No our new position is out of bounds
                // we did NOT move it, so return false
                return false;
            }
        }
        // ------------------
        public Tile GetTileAtPosition(Vector2 tilePos)
        {
            // Check if the position is within bounds
            int posX = (int)tilePos.X;
            int posY = (int)tilePos.Y;
            if (posX >= 0
                && posY >= 0
                && posX < tiles.GetLength(0) // gets the length in the X direction
                && posY < tiles.GetLength(1)) // gets the array length in the Y direction
            {
                // Yes, this coordinate is legal
                return tiles[posX, posY];
            }
            else
            {
                // NO, this coordinate is NOT legal (out of bounds of array / tile grid)
                return null;
            }

        }
        // ------------------
        public Tile GetFloorAtPosition(Vector2 tilePos)
        {
            // Check if the position is within bounds
            int posX = (int)tilePos.X;
            int posY = (int)tilePos.Y;
            if (posX >= 0
                && posY >= 0
                && posX < floorTiles.GetLength(0) // gets the length in the X direction
                && posY < floorTiles.GetLength(1)) // gets the array length in the Y direction
            {
                // Yes, this coordinate is legal
                return floorTiles[posX, posY];
            }
            else
            {
                // NO, this coordinate is NOT legal (out of bounds of array / tile grid)
                return null;
            }

        }
        // ------------------
        public void EvaluateVictory()
        {
            foreach (Tile tile in tiles)
            {
                if (tile != null && tile is Box)
                {
                    if ((tile as Box).GetOnGoal() == false)
                    {
                        // At least one box is NOT on a goal, therefore we have NOT won
                        // Exit early from this check, do nothing.
                        return;
                    }
                }
            }

            // If we got here, we know that NO boxes are NOT on goals
            // Therefore ALL boxes ARE on goals
            // Therefore we win, and should go to the next level
            // TODO: Check if we are on the last level
            loadNextLevel = true;
        }
        // ------------------
    }
}
