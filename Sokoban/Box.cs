﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Sokoban
{
    class Box : Tile
    {
        // ------------------
        // Data
        // ------------------
        private Level ourLevel;
        private bool onGoal = false;
        private Texture2D offGoalTexture;
        private Texture2D onGoalTexture;

        // ------------------
        // Behaviour
        // ------------------
        public Box(Texture2D newOffGoalTexture, Texture2D newOnGoalTexture, Level newLevel)
            : base(newOffGoalTexture)
        {
            ourLevel = newLevel;
            offGoalTexture = newOffGoalTexture;
            onGoalTexture = newOnGoalTexture;
        }
        // ------------------
        public bool TryPush(Vector2 direction)
        {
            // New position the box will be in after the push
            Vector2 newGridPos = GetTilePosition() + direction;


            // Ask the level what is in this slot already
            Tile tileInDirection = ourLevel.GetTileAtPosition(newGridPos);

            // If the target tile is a wall, we can't move there - return false.
            if (tileInDirection != null && tileInDirection is Wall)
            {
                return false;
            }
            if (tileInDirection != null && tileInDirection is Box)
            {
                return false;
            }

            // Ask the level what is on the floor in this direction
            Tile floorInDirection = ourLevel.GetFloorAtPosition(newGridPos);

            // If the target tile is a goal, change us to be in a goal
            if (floorInDirection != null && floorInDirection is Goal)
            {
                EnterGoal();
            }
            else if (onGoal == true)
            {
                ExitGoal();
            }

            // Move our tile (box) to the new position
            return ourLevel.TryMoveTile(this, newGridPos);
        }
        // ------------------
        private void EnterGoal()
        {
            onGoal = true;
            texture = onGoalTexture;
            ourLevel.EvaluateVictory();
        }
        // ------------------
        private void ExitGoal()
        {
            onGoal = false;
            texture = offGoalTexture;
        }
        // ------------------
        public bool GetOnGoal()
        {
            return onGoal;
        }
        // ------------------
    }
}
