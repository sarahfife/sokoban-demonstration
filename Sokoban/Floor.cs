﻿using Microsoft.Xna.Framework.Graphics;

namespace Sokoban
{
    class Floor : Tile
    {

        // ------------------
        // Behaviour
        // ------------------
        public Floor(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
    }
}
