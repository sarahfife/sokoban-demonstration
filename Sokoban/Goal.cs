﻿using Microsoft.Xna.Framework.Graphics;

namespace Sokoban
{
    class Goal : Tile
    {

        // ------------------
        // Behaviour
        // ------------------
        public Goal(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
    }
}
